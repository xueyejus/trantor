package database

type Submission struct {
	ID           int    `sql:"type:serial"`
	SubmissionID string `sql:"type:varchar(16)"`
	Filename     string
	Status       string
	Comment      string
	BookID       string `sql:"type:varchar(16),unique"`
	Book         *Book
}

func (db *pgDB) AddSubmission(submission Submission) (id int, err error) {
	err = db.sql.Insert(&submission)
	return submission.ID, err
}

func (db *pgDB) UpdateSubmission(id int, status string, book *Book) error {
	_, err := db.sql.Model(&Submission{}).
		Set("status = ?", status).
		Set("book_id = ?", extractID(book)).
		Where("id = ?", id).
		Update()
	return err
}

func (db *pgDB) UpdateSubmissionByBook(bookID string, status string, book *Book) error {
	_, err := db.sql.Model(&Submission{}).
		Set("status = ?", status).
		Set("book_id = ?", extractID(book)).
		Where("book_id = ?", bookID).
		Update()
	return err
}

func (db *pgDB) UpdateSubmissionComment(submissionID, bookID, comment string) error {
	_, err := db.sql.Model(&Submission{}).
		Set("comment = ?", comment).
		Where("submission_id = ?", submissionID).
		Where("book_id = ?", bookID).
		Update()
	return err
}

func (db *pgDB) GetComment(bookID string) (string, error) {
	var submission Submission
	err := db.sql.Model(&submission).
		Where("book_id = ?", bookID).
		Select()
	if err != nil {
		return "", err
	}
	return submission.Comment, nil
}

func (db *pgDB) GetSubmission(submissionID string) (submission []Submission, err error) {
	err = db.sql.Model(&submission).
		Column("Book").
		Where("submission_id = ?", submissionID).
		Select()
	return
}

func extractID(book *Book) interface{} {
	if book == nil {
		return nil
	}
	return book.ID
}
